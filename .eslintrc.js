module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true,
    jest: true,
  },/*
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    //'plugin:prettier/recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
  ],
  */
  
  extends: [
    "react-app",
    "react-app/jest",
    "airbnb",
    "airbnb-typescript",
    'prettier',
    "plugin:import/typescript",
    "plugin:prettier/recommended",
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: './tsconfig.json',
  },
  plugins: [
    'react',
    "react-hooks",
    '@typescript-eslint',
    'prettier',
  ],
  ignorePatterns: [
    '/__mocks__/*',
    '/__e2e__/*',
    '/.eslintrc.js',
    '/jest.config.js',
    '/jest-setup.js',
    '/docs/*',
    '/coverage/*',
    '/metro.config.js',
  ],
  rules: {
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single', { avoidEscape: true }],
    semi: ['error', 'always'],
    'no-empty-function': 'off',
    'react/display-name': 'off',
    'react/prop-types': 'off',
    'prettier/prettier': [
      'error',
      {
        "arrowParens": "always", // agregar los parentesis en una arrow function cuando se escribe un solo parametro
        "singleQuote": true, // cambiar las comillas dobles por simples
        "printWidth": 80,
        "semi": true, // agregar punto y coma
        "trailingComma": "all"
      },
    ],
    "arrow-body-style": "off",
    "prefer-arrow-callback": "off",
    "@typescript-eslint/no-unsafe-argument": "off",


    "import/prefer-default-export": ["off"],
    "react/react-in-jsx-scope": ["off"],
    "react/jsx-uses-react": ["off"],
    "react/prop-types": "off",
    'react-hooks/rules-of-hooks': "off",
    'react-hooks/exhaustive-deps': "off",
    'react/function-component-definition':
      [2, { namedComponents: "arrow-function" }],
    'global-require': 0,
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': [
      1,
      {
        extensions: ['.jsx','.js','.tsx', '.ts'],
      },
    ],
    'react/jsx-props-no-spreading': [
      'error',
      {
        custom: 'ignore',
      },
    ],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],
    "no-alert": 0,
    "no-console": 0,
    '@typescript-eslint/no-empty-function': 'off',
    "@typescript-eslint/no-unused-vars": "off",
    "@typescript-eslint/default-param-last": "off",
    "@typescript-eslint/no-throw-literal": 0,
    "@typescript-eslint/no-use-before-define": ["off"],
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/explicit-function-return-type": "off",
  },
};
