import { Action } from '../../interfaces/action';
import { EmployeeState } from '../../interfaces/reducers';

/** EMPLOYEE_LIST */
export const EMPLOYEE_LIST = 'capacitacion/employees/EMPLOYEE_LIST';
/**
 * Estado inicial
 */
export const initialState: EmployeeState = {
  list: [],
};

export default (state = initialState, action: Action): EmployeeState => {
  switch (action.type) {
    case EMPLOYEE_LIST: {
      const { data } = action.payload;
      return {
        ...state,
        list: data,
      };
    }

    default:
      return state;
  }
};
