import { Action } from '../../interfaces/action';
import { CharacterDTO } from '../../interfaces/starwarsService';
import { EMPLOYEE_LIST } from '../reducers/employees';

/**
 * Actualiza el listado de empleados para la capacitacion
 * @param {Array} data lista de empleados
 */
export const employeesSetList = (data: CharacterDTO[]): Action => ({
  type: EMPLOYEE_LIST,
  payload: { data },
});
