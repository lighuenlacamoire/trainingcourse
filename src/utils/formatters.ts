/**
 * Convierte la fecha o string en formato visible
 * @param value Valor para chequear a formato
 */
const formatDateString = (value: Date | string): string => {
  if (!value) {
    return '';
  }

  const date = new Date(value);

  return `${date.toLocaleString()} hs.`;
};

export { formatDateString };
