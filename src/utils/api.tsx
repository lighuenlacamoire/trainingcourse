import axios, {
  AxiosRequestConfig,
  AxiosResponse,
  Method,
  AxiosError,
} from 'axios';
import { HttpResponse } from '../interfaces/config';
import { headers, getEnvPath, Header } from '../services/api';

const axiosInstance = axios.create({
  // timeout: 1000 * 45, // Wait for 20 seconds
  baseURL: getEnvPath(),
});
// axiosInstance.defaults.timeout = 5000;

/**
 * Realiza la llamada al Endpoint
 * @param path Url relativa para agregarla al API endpoint
 * @param method Metodo HTTP ejemplo: POST, PUT, etc
 * @param headers Headers en caso de ser necesario
 * @param body Objeto a enviar en el cuerpo
 * @param suppressRedBox If true, no warning is shown on failed request
 * @returns of response body
 */
export const ApiCall = async <T,>(
  path: string,
  method: string,
  header?: Header,
  body?: any,
  suppressRedBox?: boolean,
): Promise<T> => {
  try {
    const apiRoot = getEnvPath();
    const endpoint = apiRoot + path;
    const formMethod = method as Method;
    const response = await sendRequest<T>(endpoint, formMethod, header, body);
    return response.data;
  } catch (err) {
    const error = err as AxiosError;
    const newError = handleError(error.response);
    if (!suppressRedBox) {
      logError(error, path, method);
    }
    throw newError;
  }
};

/**
 * Construye e invoca el request HTTP
 * @param path Url relativa para agregarla al API endpoint
 * @param method One of: get|post|put|delete
 * @param headers Headers en caso de ser necesario
 * @param body Objeto a enviar en el cuerpo
 */
const sendRequest = async <T,>(
  endpoint: string,
  method: Method,
  header?: Header,
  body?: any,
): Promise<AxiosResponse<T>> => {
  const formHeaders = setRequestHeaders(header);
  const requestConfig: AxiosRequestConfig = {
    method,
    url: endpoint,
    data: body,
    headers: formHeaders,
  };

  console.log('Request URL', endpoint);
  console.log('Request Body', JSON.stringify(body || ''));
  return axiosInstance.request<T>(requestConfig);
};

/**
 * Crea los cabezales para el request HTTP
 * @param header Headers en caso de ser necesario
 * @param token token de la app
 */
export const setRequestHeaders = (header?: Header, token?: string): Header => {
  const formHeaders = header || headers;

  return formHeaders;
};

const handleError = (error: any): HttpResponse => {
  let erMessage = 'Ha ocurrido un error';
  let erCode = 500;
  let erStatus = 500;
  if (error) {
    if (error.message && error.message.length > 0) {
      erMessage = error.message;
    }

    if (error.statusText && error.statusText.length > 0) {
      erMessage = error.statusText;
    }

    if (error.data && error.data.data) {
      erMessage = error.data?.data?.errMessage;
      erCode = parseInt(error.data?.data?.errNumber || '0', 10);
    }

    if (error.status) {
      erStatus = error.status;
    }
  }

  return {
    code: erCode,
    status: erStatus,
    message: erMessage,
  };
};

/**
 * Se intenta recuperar el error de HTTP o de ejecucion
 */
const logError = (error: any, endpoint: string, method: string) => {
  if (error.status) {
    // eslint-disable-next-line no-underscore-dangle
    const summary = `(${error.status} ${error.statusText}): ${error._bodyInit}`;
    console.log(
      `API request ${method.toUpperCase()} ${endpoint} responded with ${summary}`,
    );
  } else {
    console.log(
      `API request ${method.toUpperCase()} ${endpoint} failed with message "${
        error.message
      }"`,
    );
  }
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {};
