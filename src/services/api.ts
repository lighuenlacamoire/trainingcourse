interface EnvPath {
  [name: string]: string;
}

const path: EnvPath = {
  PROD: 'https://swapi.dev/',
  PRE: 'https://swapi.dev/',
  QA: 'https://swapi.dev/',
  DEV: 'https://swapi.dev/',
};

export interface Header {
  [key: string]: string;
}
export const headers: Header = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

/** * Variables de ambiente */
export const getEnvPath = (): string => path.DEV;

const peopleController = 'api/people';

/**
 * Lista de Endpoints
 */
export const paths = {
  app: {
    employees: {
      list: {
        resource: (pages) =>
          `${peopleController}/${pages ? `?page=${pages}` : ''}`,
        method: 'GET',
      },
    },
  },
};

export default {};
