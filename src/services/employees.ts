import { paths } from './api';
import { ApiCall } from '../utils/api';
import { CharacterDTO, StarWarsListDTO } from '../interfaces/starwarsService';

/**
 * Consulta el listado de Empleados en capacitacion
 * @param pages cantidad de paginas
 */
export const employeesListRequest = (pages?: string) =>
  ApiCall<StarWarsListDTO<CharacterDTO>>(
    paths.app.employees.list.resource(pages),
    paths.app.employees.list.method,
    undefined,
    undefined,
    undefined,
  );
