import { ModalMessage } from './buttons';
import { MessageTitle } from './config';
import { CharacterDTO } from './starwarsService';

export interface StatusState {
  loading?: boolean;
  active?: boolean;
  failure?: MessageTitle;
  closeApp: boolean;
}

export interface EmployeeState {
  list: CharacterDTO[];
}

export interface NotificationState {
  message?: ModalMessage;
}
