import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Oval } from 'react-loader-spinner';
// import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import { RootState } from '../redux/store';

const LoadingIndicator = () => {
  /** Dispatch de Redux */
  const { loading } = useSelector((state: RootState) => state.status);
  const [activity, setActivity] = useState(loading);
  /**
   * Verifica si existe algun elemento cargando
   *
   */
  const isLoading = () => {
    setActivity(loading);
  };

  useEffect(() => {
    isLoading();
  }, [loading]);

  return activity ? (
    <div className="loading-overlay">
      <div className="loading-container">
        <Oval color="#307ecc" ariaLabel="loading-indicator" />
      </div>
    </div>
  ) : null;
};

export default LoadingIndicator;
