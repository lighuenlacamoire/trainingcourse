import React from 'react';
import { useNavigate } from 'react-router-dom';
import '../../App.css';
import { Button } from 'react-bootstrap';
import logo from '../../logo.svg';

const Home = (): JSX.Element => {
  const navigate = useNavigate();

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Button
          onClick={() => {
            navigate('/Employees');
          }}>
          Ver Listado
        </Button>
      </header>
    </div>
  );
};

export default Home;
